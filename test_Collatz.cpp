// ----------------
// test_Collatz.cpp
// ----------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/docs/reference/assertions.md

// --------
// includes
// --------

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;

// --------------
// CollatzFixture
// --------------

TEST(CollatzFixture, collatz_eval_0) {
    const tuple_type_1 test1In = {1, 10};
    const tuple_type_2 test1Out = collatz_eval(test1In);
    const tuple_type_2 test1Check = {1, 10, 20};
    ASSERT_TRUE(test1Out == test1Check);
}

TEST(CollatzFixture, collatz_eval_1) {
    const tuple_type_1 test2In = {100, 200};
    const tuple_type_2 test2Out = collatz_eval(test2In);
    const tuple_type_2 test2Check = {100, 200, 125};
    ASSERT_TRUE(test2Out == test2Check);
}

TEST(CollatzFixture, collatz_eval_2) {
    const tuple_type_1 test3In = {201, 210};
    const tuple_type_2 test3Out = collatz_eval(test3In);
    const tuple_type_2 test3Check = {201, 210, 89};
    ASSERT_TRUE(test3Out == test3Check);
}

TEST(CollatzFixture, collatz_eval_3) {
    const tuple_type_1 test4In = {900, 1000};
    const tuple_type_2 test4Out = collatz_eval(test4In);
    const tuple_type_2 test4Check = {900, 1000, 174};
    ASSERT_TRUE(test4Out == test4Check);
}

TEST(CollatzFixture, collatz_eval_4) {
    const tuple_type_1 test5In = {808, 250};
    const tuple_type_2 test5Out = collatz_eval(test5In);
    const tuple_type_2 test5Check = {808, 250, 171};
    ASSERT_TRUE(test5Out == test5Check);
}

TEST(CollatzFixture, collatz_eval_5) {
    const tuple_type_1 test6In = {74, 659};
    const tuple_type_2 test6Out = collatz_eval(test6In);
    const tuple_type_2 test6Check = {74, 659, 145};
    ASSERT_TRUE(test6Out == test6Check);
}

TEST(CollatzFixture, collatz_eval_6) {
    const tuple_type_1 test7In = {931, 273};
    const tuple_type_2 test7Out = collatz_eval(test7In);
    const tuple_type_2 test7Check = {931, 273, 179};
    ASSERT_TRUE(test7Out == test7Check);
}

TEST(CollatzFixture, collatz_eval_7) {
    const tuple_type_1 test8In = {545, 879};
    const tuple_type_2 test8Out = collatz_eval(test8In);
    const tuple_type_2 test8Check = {545, 879, 179};
    ASSERT_TRUE(test8Out == test8Check);
}

TEST(CollatzFixture, collatz_eval_8) {
    const tuple_type_1 test9In = {924, 710};
    const tuple_type_2 test9Out = collatz_eval(test9In);
    const tuple_type_2 test9Check = {924, 710, 179};
    ASSERT_TRUE(test9Out == test9Check);
}

TEST(CollatzFixture, collatz_eval_9) {
    const tuple_type_1 test10In = {441, 166};
    const tuple_type_2 test10Out = collatz_eval(test10In);
    const tuple_type_2 test10Check = {441, 166, 144};
    ASSERT_TRUE(test10Out == test10Check);
}

TEST(CollatzFixture, collatz_eval_10) {
    const tuple_type_1 test11In = {493, 43};
    const tuple_type_2 test11Out = collatz_eval(test11In);
    const tuple_type_2 test11Check = {493, 43, 144};
    ASSERT_TRUE(test11Out == test11Check);
}

TEST(CollatzFixture, collatz_eval_11) {
    const tuple_type_1 test12In = {988, 504};
    const tuple_type_2 test12Out = collatz_eval(test12In);
    const tuple_type_2 test12Check = {988, 504, 179};
    ASSERT_TRUE(test12Out == test12Check);
}

TEST(CollatzFixture, collatz_eval_12) {
    const tuple_type_1 test13In = {988, 504};
    const tuple_type_2 test13Out = collatz_eval(test13In);
    const tuple_type_2 test13Check = {988, 504, 179};
    ASSERT_TRUE(test13Out == test13Check);
}

TEST(CollatzFixture, collatz_eval_13) {
    const tuple_type_1 test14In = {328, 730};
    const tuple_type_2 test14Out = collatz_eval(test14In);
    const tuple_type_2 test14Check = {328, 730, 171};
    ASSERT_TRUE(test14Out == test14Check);
}

TEST(CollatzFixture, collatz_eval_14) {
    const tuple_type_1 test15In = {841, 613};
    const tuple_type_2 test15Out = collatz_eval(test15In);
    const tuple_type_2 test15Check = {841, 613, 171};
    ASSERT_TRUE(test15Out == test15Check);
}

TEST(CollatzFixture, collatz_eval_15) {
    const tuple_type_1 test16In = {304, 170};
    const tuple_type_2 test16Out = collatz_eval(test16In);
    const tuple_type_2 test16Check = {304, 170, 128};
    ASSERT_TRUE(test16Out == test16Check);
}

TEST(CollatzFixture, collatz_eval_16) {
    const tuple_type_1 test17In = {710, 158};
    const tuple_type_2 test17Out = collatz_eval(test17In);
    const tuple_type_2 test17Check = {710, 158, 171};
    ASSERT_TRUE(test17Out == test17Check);
}

TEST(CollatzFixture, collatz_eval_17) {
    const tuple_type_1 test18In = {561, 934};
    const tuple_type_2 test18Out = collatz_eval(test18In);
    const tuple_type_2 test18Check = {561, 934, 179};
    ASSERT_TRUE(test18Out == test18Check);
}

TEST(CollatzFixture, collatz_eval_18) {
    const tuple_type_1 test19In = {100, 279};
    const tuple_type_2 test19Out = collatz_eval(test19In);
    const tuple_type_2 test19Check = {100, 279, 128};
    ASSERT_TRUE(test19Out == test19Check);
}

TEST(CollatzFixture, collatz_eval_19) {
    const tuple_type_1 test20In = {817, 336};
    const tuple_type_2 test20Out = collatz_eval(test20In);
    const tuple_type_2 test20Check = {817, 336, 171};
    ASSERT_TRUE(test20Out == test20Check);
}

TEST(CollatzFixture, collatz_eval_20) {
    const tuple_type_1 test21In = {98, 827};
    const tuple_type_2 test21Out = collatz_eval(test21In);
    const tuple_type_2 test21Check = {98, 827, 171};
    ASSERT_TRUE(test21Out == test21Check);
}

TEST(CollatzFixture, collatz_eval_21) {
    const tuple_type_1 test22In = {513, 268};
    const tuple_type_2 test22Out = collatz_eval(test22In);
    const tuple_type_2 test22Check = {513, 268, 144};
    ASSERT_TRUE(test22Out == test22Check);
}
