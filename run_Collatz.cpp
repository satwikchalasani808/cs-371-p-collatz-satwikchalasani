// ---------------
// run_Collatz.cpp
// ---------------

// --------
// includes
// --------

#include <iostream> // cin, cout, endl

#include "Collatz.hpp"

// ------------
// collatz_read
// ------------

tuple_type_1 collatz_read () {
    int i, j;
    cin >> i >> j;
    return {i, j};
}

// -------------
// collatz_print
// -------------

void collatz_print (const tuple_type_2& t2) {
    auto [i, j, v] = t2;
    cout << i << " " << j << " " << v << endl;
}

// ----
// main
// ----

int main () {
    while (true) {
        tuple_type_1 t1 = collatz_read();
        if (!cin)
            break;
        collatz_print(collatz_eval(t1));
    }
    return 0;
}

/*
sample input

1 10
100 200
201 210
900 1000
*/

/*
sample output

1 10 20
100 200 125
201 210 89
900 1000 174
*/
