# CS371p: Object-Oriented Programming Collatz Repo

* Name: Satwik Chalasani

* EID: sc66594

* GitLab ID: satwikchalasani808

* HackerRank ID: satwikchalasani1

* Git SHA: c9e50da7c573196a49205699cc54906116882fe6

* GitLab Pipelines: https://gitlab.com/satwikchalasani808/cs-371-p-collatz-satwikchalasani

* Estimated completion time: 7.5 hrs

* Actual completion time: 10 hrs

* Comments: N/A
