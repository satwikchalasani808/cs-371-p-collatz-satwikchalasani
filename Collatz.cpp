// -----------
// Collatz.cpp
// -----------

// --------
// includes
// --------

#include <cassert> // assert
#include <unordered_map>
#include "Collatz.hpp"
// ------------
// collatz_eval
// ------------

tuple_type_2 collatz_eval (const tuple_type_1& t1) {
    auto [input1, input2] = t1;
    assert(input1 > 0);
    assert(input2 > 0);
    // Use map for caching - Key: N, Value: Number of Cycles
    unordered_map<int, int> length_values;
    int maximum_length = 1;
    // Switching the values if x happens to be greater than y on input
    int temporary_first_value = input1;
    int temporary_second_value = input2;
    if (input1 > input2) {
        input1 = input2;
        input2 = temporary_first_value;
    }
    for (int n = input1; n <= input2; ++n) {
        long long temp = n;
        int key = n;
        int counter = 1;
        while (temp > 1) {
            // Check if the n we are calculating has already been calculated before
            if (length_values.count(temp)) {
                counter += length_values[temp] - 1;
                break;
            }
            else if ((temp % 2) == 0) {
                // Dividing by 2 until temp is not even to speed up execution
                while ((temp % 2) == 0) {
                    temp = (temp >> 1);
                    counter += 1;
                }
            }
            else {
                temp = temp + (temp >> 1) + 1;
                counter += 2;
            }
        }
        // Putting n into hashmap if not already there with cycle length
        if (!length_values.count(key)) {
            length_values[key] = counter;
        }
        maximum_length = max(counter, maximum_length);
    }
    assert(maximum_length > 0);
    return {temporary_first_value, temporary_second_value, maximum_length};
}



